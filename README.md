# hex-encode-decode

Hex encode & decode string

本库作者[坚果](https://gitee.com/jianguo888/)，感谢大家对坚果派的支持。

## 一、下载安装

```
ohpm install @nutpi/hex_encode_decode
```

OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 二、使用

```js
import hexCodec from "@nutpi/hex_encode_decode"

```



```js
import hexCodec from "@nutpi/hex_encode_decode"

@Entry
@Component
struct Index {
    @State message: string = '坚果派';

    build() {
        Row() {
            Column() {
                Text(hexCodec.encode('hello'))
                    .fontSize(50)
                    .fontWeight(FontWeight.Bold)

                Text(hexCodec.decode('68656c6c6f'))
                    .fontSize(50)
                    .fontWeight(FontWeight.Bold)

            }
            .width('100%')
        }
        .height('100%')
    }
}
```



##  三、开源协议

本项目基于 [Apache](https://gitee.com/nutpi/relationship/blob/master/library/LICENSE) ，请自由地享受和参与开源。感谢大家对坚果派的支持。









